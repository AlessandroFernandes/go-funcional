package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const nMonotoring = 2
const monotoringTime = 5

func main() {
	player()

	println("\n Welcome..........................\n")
	println("Please, select option")

	for {
		menu()

		var option int
		fmt.Scan(&option)

		switch option {
		case 1:
			siteStatus()
		case 2:
			fmt.Println("Log system")
			readLog()
		case 0:
			fmt.Println("********************* Goodbye")
			os.Exit(0)
		default:
			fmt.Println("********************* Error")
			os.Exit(-1)
		}
	}

}

func player() {
	name := "Alessandro"
	var version float32 = 0.1

	fmt.Println("\n**************************************")
	fmt.Println("Administrator name:", name)
	fmt.Println("********************* system version", version)
	fmt.Println("")
}

func menu() {
	fmt.Println("\n1 - Status websites")
	fmt.Println("2 - Log status")
	fmt.Println("0 - Exit system")
}

func siteStatus() {
	sites := getFileSites()
	for i := 0; i < nMonotoring; i++ {
		for _, site := range sites {
			status, _ := http.Get(site)
			if status.StatusCode == 200 {
				fmt.Println("Site:", site, " - Status", status.StatusCode)
				createLog(site, true)
			} else {
				fmt.Println("Site:", site, " - Status", status.StatusCode)
				createLog(site, false)
			}
		}
		fmt.Println("")
		time.Sleep(monotoringTime * time.Second)
	}
}

func getFileSites() []string {
	var sites []string
	file, err := os.Open("sites.txt")

	if err != nil {
		fmt.Println("Load error...")
	}

	item := bufio.NewReader(file)

	for {
		linha, err := item.ReadString('\n')
		sites = append(sites, strings.TrimSpace(linha))
		if err == io.EOF {
			break
		}

	}
	file.Close()
	return sites
}

func createLog(site string, status bool) {
	file, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		fmt.Println("File error")
	}

	file.WriteString("[" + time.Now().Format("02/01/2006 15:04:05") + "] " + site + " - Status: " + strconv.FormatBool(status) + "\n")
	file.Close()
}

func readLog() {
	file, err := ioutil.ReadFile("log.txt")

	if err != nil {
		fmt.Println("Error read file...")
	}

	fmt.Println(string(file))
}
